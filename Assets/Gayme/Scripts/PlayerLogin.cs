using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerLogin : MonoBehaviour
{
    public InputField usernameInputField;
    
    public InputField PasswordInputField;
    public void OnLogin()
    {
        LoginWithPlayFabRequest request = new LoginWithPlayFabRequest()
        {
            Username = usernameInputField.text.Replace(" ", ""),
            Password = PasswordInputField.text,
            TitleId = PlayFabSettings.TitleId

        };
        PlayFabClientAPI.LoginWithPlayFab(request, OnResultCallback, ErrorCallback);
    }
    private void ErrorCallback(PlayFabError error)
    {
        Debug.Log("error:( " + error.ErrorMessage);
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        string Message = "";
        if (error.ErrorDetails != null)
        {
            foreach (var i in error.ErrorDetails)
            {
                foreach (var j in i.Value)
                {
                    if (j.Contains("Username contains invalid characters"))
                    {
                        Message = Message + "El Usuario contiene caracteres inválidos" + "\n";
                    }
                    else if (j.Contains("Username must be between 3 and 20 characters"))
                    {
                        Message = Message + "El Usuario debe tener entre 3 y 20 caracteres" + "\n";
                    }
                    else if (j.Contains("User name already exists"))
                    {
                        Message = Message + "El Usuario no está disponible" + "\n";
                    }
                    else if (j.Contains("Email address is not valid"))
                    {
                        Message = Message + "El Correo no es válido" + "\n";
                    }
                    else if (j.Contains("Email address already exists"))
                    {
                        Message = Message + "El Correo no está disponible" + "\n";
                    }
                    else if (j.Contains("Password must be between 6 and 100 characters"))
                    {
                        Message = Message + "La contraseña debe tener entre 6 y 100 caracteres" + "\n";
                    }
                    else if (j.Contains("User not found"))
                    {
                        Message = Message + "Usuario no encontrado" + "\n";
                    }
                    Debug.Log("********" + j);
                }
            }
        }
        if (Message == "")
        {
            Message = "Verifica que todos tus datos esten debidamente llenados e intenta de nuevo";
        }
        NotificationPanel.Instance.ShowNotification(Message);
        //InformationController._instance.ShowPanelInformation(Message);

        //SceneController._instance.CloseStaticLoadingPanel();
    }
    private void OnResultCallback(LoginResult result)
    {
        PlayerPrefs.SetString("PlayFabId", result.PlayFabId);
        
        SceneManager.LoadScene("MainMenu");
        Debug.Log("inicio de sesion juajua");
    }
    
}
