using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ServerModels;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    [SerializeField] private GameObject _content;
    [SerializeField] private string _leaderboard;
    [SerializeField] private GameObject _itemPrefap;
    void Start()
    {
        GetLeaderBoard(0,10);
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            UpdateLeaderBoard(10);
        }
        
    }
    public void UpdateLeaderBoard(int score)
    {
        List<StatisticUpdate> stats = new List<StatisticUpdate>();
        stats.Add(new StatisticUpdate()
        {
            StatisticName =  _leaderboard,
            Value = 10
        });

        UpdatePlayerStatisticsRequest request = new UpdatePlayerStatisticsRequest()
        {
            Statistics = stats,
            PlayFabId = PlayerPrefs.GetString("PlayFabId")
        };
        PlayFabServerAPI.UpdatePlayerStatistics(request, ResultCallback, ErrorCallbackUpdateLeaderboard);
    }

    public void GetLeaderBoard(int startPosition, int maxResultsCount)
    {
        GetLeaderboardRequest request = new GetLeaderboardRequest()
        {
            StartPosition = startPosition,
            StatisticName = _leaderboard,
            MaxResultsCount = maxResultsCount
        };
        
        PlayFabServerAPI.GetLeaderboard(request, ResultCallback, ErrorCallback);
    }
    void ErrorCallbackUpdateLeaderboard(PlayFabError obj)
    {
        Debug.Log(obj.ErrorMessage);
    }
    void ErrorCallback(PlayFabError obj)
    {
        
    }
    void ResultCallback(UpdatePlayerStatisticsResult obj)
    {
        
        
        
        Debug.Log("leaderboard actualizado");
    }
    void ResultCallback(GetLeaderboardResult obj)
    {
        foreach (var user in obj.Leaderboard)
        {
            GameObject newItem = Instantiate(_itemPrefap, _content.transform.position, Quaternion.identity, _content.transform);
            newItem.GetComponent<itemLeaderBoard>().SetInitialValues((user.Position+1).ToString(), user.DisplayName, user.StatValue.ToString());
            Debug.Log("Posicion numero "+  (user.Position+1) + " Player: "+ user.DisplayName + " score: " + user.StatValue);
        }
        
        
        Debug.Log("leaderboard obtenido");
    }
}
