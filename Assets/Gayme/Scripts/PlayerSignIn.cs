using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
public class PlayerSignIn : MonoBehaviour
{
    public InputField usernameInputField;
    public InputField emailInputField;
    public InputField PasswordInputField;
    public InputField ageInputField;
    public InputField cityInputField;
    void Start()
    {
        //OnRegister();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnRegister()
    {
        RegisterPlayFabUserRequest request = new RegisterPlayFabUserRequest()
        {
            Email = emailInputField.text,
            Username = usernameInputField.text.Replace(" ", ""),
            Password = PasswordInputField.text,
            TitleId = PlayFabSettings.TitleId,
            DisplayName = usernameInputField.text
        };
        PlayFabClientAPI.RegisterPlayFabUser(request, ResultCallback, ErrorCallback);
        
    }
    private void ErrorCallback(PlayFabError error)
    {
        Debug.Log("error:( " + error.ErrorMessage);
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        string Message = "";
        if (error.ErrorDetails != null)
        {
            foreach (var i in error.ErrorDetails)
            {
                foreach (var j in i.Value)
                {
                    if (j.Contains("Username contains invalid characters"))
                    {
                        Message = Message + "El Usuario contiene caracteres inválidos" + "\n";
                    }
                    else if (j.Contains("Username must be between 3 and 20 characters"))
                    {
                        Message = Message + "El Usuario debe tener entre 3 y 20 caracteres" + "\n";
                    }
                    else if (j.Contains("User name already exists"))
                    {
                        Message = Message + "El Usuario no está disponible" + "\n";
                    }
                    else if (j.Contains("Email address is not valid"))
                    {
                        Message = Message + "El Correo no es válido" + "\n";
                    }
                    else if (j.Contains("Email address already exists"))
                    {
                        Message = Message + "El Correo no está disponible" + "\n";
                    }
                    else if (j.Contains("Password must be between 6 and 100 characters"))
                    {
                        Message = Message + "La contraseña debe tener entre 6 y 100 caracteres" + "\n";
                    }
                    else if (j.Contains("User not found"))
                    {
                        Message = Message + "Usuario no encontrado" + "\n";
                    }
                    Debug.Log("********" + j);
                }
            }
        }
        if (Message == "")
        {
            Message = "Verifica que todos tus datos esten debidamente llenados e intenta de nuevo";
        }
        NotificationPanel.Instance.ShowNotification(Message);
    }
    private void ResultCallback(RegisterPlayFabUserResult result)
    {
        Debug.Log("Registrado :D");
        SetUserData();
    }

    public void SetUserData()
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>()
            {
                {"Nombre de usuarioh", usernameInputField.text},
                {"i-meil", emailInputField.text},
                
                {"Edah", ageInputField.text},
                {"Ciudah", cityInputField.text}
            }
        };
        PlayFabClientAPI.UpdateUserData(request, ResultCallback, ErrorCallback2);
    }

    private void ErrorCallback2(PlayFabError obj)
    {
        Debug.Log("Datos: no actualizados aaaa");
    }

    private void ResultCallback(UpdateUserDataResult obj)
    {
        Debug.Log("Datos actualizados a");
    }
   
}
