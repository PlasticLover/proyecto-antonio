using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class itemLeaderBoard : MonoBehaviour
{
    [SerializeField] private Text _positionText;
    [SerializeField] private Text _userNameText;
    [SerializeField] private Text _scoreText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetInitialValues(string position, string username, string score)
    {
        _positionText.text = position;
        _userNameText.text = username;
        _scoreText.text = score;
    }
}
