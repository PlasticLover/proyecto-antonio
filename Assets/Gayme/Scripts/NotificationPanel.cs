using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.UI;

public class NotificationPanel : MonoBehaviour
{
    public static NotificationPanel Instance;
    [SerializeField] private GameObject _panel;
    [SerializeField] private Text _notificationText;
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    public void ShowNotification(string message)
    {
        _panel.SetActive(true);
        _notificationText.text = message;
    }
}
